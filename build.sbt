name := "uploader"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "5.1.18",
  "com.typesafe.slick" %% "slick" % "1.0.1",
  jdbc,
  cache
)     

play.Project.playScalaSettings
