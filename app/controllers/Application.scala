package controllers

import scala.slick.driver.MySQLDriver.simple._
import play.api._
import play.api.mvc.{Session => PlaySession,_}
import play.api.data.Forms._
import play.api.data._
import play.api.Play.current
import play.api.db.{DB => _, _}
import scala.collection.mutable._
import models._
import java.io.FileInputStream
import java.sql.Blob
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.net.URLEncoder

object Application extends Controller with Secured {  
  
  protected val sessionName : String = "app"
  
  protected val unauthorized : Call = routes.Application.login
  
  protected val authorized : Call = routes.Application.index
  
  protected def auth(username: String, password: String) : Boolean = username == "komabakai" && password == "komabakai"
  
  class MySimpleResult(result: SimpleResult){
    def addHeader(header : (String, String)) = {
      val headers = result.header.headers.toSeq :+ header
      result.withHeaders(headers: _*)
    }
    
    def byDownload(filename: String, contentType: String = null) : SimpleResult = {        
        val encodedFilename = URLEncoder.encode( filename,"UTF8")
        
        val fileResult = addHeader(("Content-Disposition", "attachment; filename*=UTF-8''"+encodedFilename))
        if(contentType != null && contentType != ""){
          fileResult.addHeader(("Content-Type", contentType))
        } else {
          fileResult
        }
    }
  }
  implicit def simpleresultconverter(result: SimpleResult) : MySimpleResult= new MySimpleResult(result)
  
  def index = IsAuthenticated { implicit request =>
    val files = FileInfos.findAll
    Ok(views.html.index(files))
  }
  
  def download(id: Int) = IsAuthenticated { implicit request =>
    FileInfos.findById(id) match {
      case None => NotFound("not found")
      case Some(metadata) => {
        val data = Files.findById(id).get.file
    
        val filename = metadata.title + "." + metadata.extention

        Ok(data).byDownload(filename, getContentType(metadata.extention))
      }
    }
  }
  
  private def getContentType(ext: String) = ext match {
    case "zip" => "application/zip"
    case "pdf" => "applicatino/pdf"
    case "xls" => "application/vnd.ms-excel"
    case "xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    case "doc" => "application/msword"
    case "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    case "ppt" => "application/vnd.ms-powerpoint"
    case "pptx" => "application/vnd.openxmlformats-officedocument.presentationml.presentation"
    case "bmp" => "image/x-bmp"
    case "gif" => "image/gif"
    case "jpg" => "image/jpeg"
    case "jpeg" => "image/jpeg"
    case "png" => "image/png"
    case "mp3" => "audio/mpeg"
    case "wav" => "audio/wav"
    case "mp4" => "vide/mpeg"
    case "mpeg" => "vide/mpeg"
    case "wmv" => "video/x-ms-wmv"
    case _ => null
  }
}
