package controllers

import play.api.mvc._
import java.util.Date

trait Secured { this : Controller => 
  
  protected val sessionName : String
  
  protected val unauthorized : Call
  
  protected val authorized : Call
  
  protected def auth(username: String, password: String) : Boolean
  
  protected val title : String = "login"
  
  protected val period = 24 * 60 * 60 * 1000
  
  private def logintime(request: RequestHeader) = request.session.get(sessionName).filter{ logintime =>
    val limit = logintime.toLong + period
    
    new Date().getTime() < limit
  }

  private def onUnauthorized(request: RequestHeader) = Results.Redirect(unauthorized)

  def IsAuthenticated(f: => Request[AnyContent] => Result): EssentialAction = 
    Security.Authenticated(logintime, onUnauthorized) { _ =>
      Action(request => f(request))
    }
  
  def IsAuthenticated[A](bodyParser : BodyParser[A])(f: => Request[A] => Result): EssentialAction =  
    Security.Authenticated(logintime, onUnauthorized) { _ =>
      Action(bodyParser)(request => f(request))
    }

  
  def login = Action {
    Ok(views.html.login(title))
  }
  
  def authenticate = Action { implicit request =>
    val flag =  request.body.asFormUrlEncoded.map{form => 
      val username = form("username").headOption.getOrElse("")
      val password = form("password").headOption.getOrElse("")
      
      auth(username, password)
    }.getOrElse(false)
    
    if(flag){
      val session = request.session + (sessionName, new Date().getTime.toString)
      Redirect(authorized).withSession(session)
    } else {
      BadRequest(views.html.login(title, true))
    }
  }
}