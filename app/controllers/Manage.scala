package controllers

import scala.slick.driver.MySQLDriver.simple._
import play.api._
import play.api.mvc.{Session => PlaySession,_}
import play.api.data.Forms._
import play.api.data._
import play.api.Play.current
import play.api.db.{DB => _, _}
import scala.collection.mutable._
import java.io.FileInputStream
import java.sql.Blob
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import scala.util.Random
import java.io.ByteArrayOutputStream
import java.util.zip.ZipOutputStream
import java.util.zip.ZipEntry
import java.io.FileOutputStream
import javassist.bytecode.ByteArray
import java.io.ByteArrayInputStream
import models._
import java.sql.Time
import java.sql.Timestamp
import java.util.Calendar
import java.util.TimeZone
import java.util.Locale

object Manage extends Controller with Secured{
  protected override val title: String = "manage login"
  
  protected val sessionName : String = "manage"
  
  protected val unauthorized : Call = routes.Manage.login
  
  protected val authorized : Call = routes.Manage.index
  
  protected def auth(username: String, password: String) : Boolean = username == "undokai" && password == "kyantoku"
    
  def index = IsAuthenticated { implicit request =>
    Ok(views.html.manage.index())
  }
  
  def file2bytes(file : File) = {
    val data = new Array[Byte](file.length.toInt)
    
    val inputStream = new FileInputStream(file)
    inputStream.read(data)
    inputStream.close()
    
    data
  }

  def upload = IsAuthenticated(parse.multipartFormData) { implicit request =>
    val form = request.body.asFormUrlEncoded
        
    request.body.file("file") match {
      case Some(file) => {
        val fileContent = file2bytes(file.ref.file)
        val extention = file.filename.split("\\.").last
        val title = form("title")(0)          
        val comment = form("comment")(0)
        val date = new Timestamp(Calendar.getInstance().getTimeInMillis())
        FileInfos.insert(FileInfo(None, title, extention, comment, date), fileContent)
    
        Redirect(routes.Manage.index).flashing(("success" ,"success"))
      }
      case None => {
        Redirect(routes.Manage.index).flashing(("success" ,"success"))
      }
    }
  }

  def test = IsAuthenticated{ implicit request =>
    if(Play.isDev){
      Ok(FileInfos.ddl.createStatements.mkString("\n") + ";\n" + Files.ddl.createStatements.mkString("\n") + ";")
    } else {
      Forbidden
    }
  }
}
