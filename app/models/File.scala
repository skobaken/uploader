package models

import scala.slick.driver.MySQLDriver.simple._
import play.api.Play.current

case class File(id: Int, file :Array[Byte])

object Files extends Table[File]("files"){
  def id = column[Int]("id", O.PrimaryKey)
  def file = column[Array[Byte]]("file", O.DBType("longblob"))
  def * = (id ~ file ) <> (File.apply _, File.unapply _)
  
  def findById(id : Int) : Option[File] = DB.withSession{ implicit session =>
    Query(Files).filter(_.id === id).list.headOption
  }
}
