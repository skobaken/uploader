package models

import scala.slick.driver.MySQLDriver.simple._
import play.api.Play.current
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List

case class Setting(id: Option[Int], name: String, value: String)

object Settings extends Table[Setting]("settings"){
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def value = column[String]("value")
  def * = (id.? ~ name ~ value) <> (Setting.apply _, Setting.unapply _)
  
  def findAll = DB.withSession{ implicit session => 
    Query(Settings).list
  }
  
  def getValue(name: String) = DB.withSession{ implicit session =>
    Query(Settings).filter(_.name === name).list.headOption.map(_.value)
  }
  
  def getValue[A](name: String, f: String => A) = DB.withSession{ implicit session =>
    Query(Settings).filter(_.name === name).list.headOption.map(a => f(a.value))
  }
  
  def updateValue[A](name: String, value: A) = DB.withTransaction{ implicit session =>
    val count = Query(Settings).filter(_.name === name).map(_.value).update(value.toString)
    
    if(count != 1){
      Query(Settings).filter(_.name === name).delete
      Settings.insert(Setting(None, name, value.toString))
    }
  }
}
