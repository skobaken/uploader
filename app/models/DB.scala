package models

import scala.slick.driver.MySQLDriver.simple._
import play.api.db.{DB => PlayDB}
import play.api.Play.current

object DB {
  def apply(name: String) = {
    db = Database.forDataSource(PlayDB.getDataSource(name))
  }
  
  var db = Database.forDataSource(PlayDB.getDataSource("default"))
  
  def withSession[A](f: Session => A) = db.withSession(f)
  
  def withTransaction[A](f: Session => A) = db.withTransaction(f)
}