package models

import scala.slick.driver.MySQLDriver.simple._
import play.api.Play.current
import play.api.Logger
import java.sql.Time
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Locale
import java.util.Calendar
import java.util.TimeZone

case class FileInfo(id: Option[Int], title: String, extention: String, comment: String, createdDatetime: Timestamp){
  val DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss"
  
  def datetimeStr = {
    val cal = Calendar.getInstance
    cal.setTime(createdDatetime)
    cal.setTimeZone(TimeZone.getTimeZone("JST"))
    
    val y = cal.get(Calendar.YEAR)
    val mon = cal.get(Calendar.MONTH)
    val d = cal.get(Calendar.DAY_OF_MONTH)
    val h = cal.get(Calendar.HOUR_OF_DAY)
    val min = cal.get(Calendar.MINUTE)
    val s = cal.get(Calendar.SECOND)
    
    "%04d/%02d/%02d %02d:%02d:%02d".format(y, mon, d, h, min, s)
  }
}

object FileInfos extends Table[FileInfo]("fileinfos"){
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title = column[String]("title")
  def extention = column[String]("extention")
  def comment = column[String]("comment")
  def createdDatetime = column[Timestamp]("created_datetime")
  def * = (id.? ~ title ~ extention ~ comment ~ createdDatetime) <> (FileInfo.apply _, FileInfo.unapply _)
  def ins = * returning id
  
  def findAll : List[FileInfo] = DB.withSession{ implicit session =>
    Query(FileInfos).sortBy(_.createdDatetime.desc).list
  }
  
  def findById(id : Int) : Option[FileInfo] = DB.withSession{ implicit session =>
    Query(FileInfos).filter(_.id === id).list.headOption
  }
  
  def insert(fileInfo: FileInfo, file: Array[Byte]) : Int= DB.withTransaction{implicit session => 
    val id = FileInfos.ins.insert(fileInfo)
    Files.insert(File(id, file))
    id
  }
}
